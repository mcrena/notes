package com.test.notes.mvp

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import moxy.MvpPresenter
import moxy.MvpView

/**
 * Created by Dashkevich Makar on 20/06/2020.
 * mcrena@mail.ru
 */
open class BasePresenter<V : BaseView> : MvpPresenter<V>() {
    protected val compositeDisposable = CompositeDisposable()

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

}