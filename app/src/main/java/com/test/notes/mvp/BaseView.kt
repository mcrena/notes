package com.test.notes.mvp

import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution

/**
 * Created by Dashkevich Makar on 20/06/2020.
 * mcrena@mail.ru
 */
interface BaseView : MvpView {

    @OneExecution
    fun showToast(resId: Int)

    @OneExecution
    fun showSnackbar(message: String)
}