package com.test.notes.di.modules

import android.content.Context
import androidx.room.Room
import com.test.notes.model.db.NotesDatabase
import com.test.notes.model.repository.NotesRepository
import com.test.notes.model.repository.NotesRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Dashkevich Makar on 22/06/2020.
 * mcrena@mail.ru
 */
@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun database(context: Context): NotesDatabase {
        return Room.databaseBuilder(context, NotesDatabase::class.java, "database").build()
    }

    @Provides
    @Singleton
    fun notesRepository(db: NotesDatabase): NotesRepository {
        return NotesRepositoryImpl(db)
    }
}