package com.test.notes.di.scopes

import javax.inject.Scope

/**
 * Created by Dashkevich Makar on 19/06/2020.
 * mcrena@mail.ru
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope