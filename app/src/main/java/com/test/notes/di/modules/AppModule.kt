package com.test.notes.di.modules

import android.content.Context
import androidx.room.Room
import com.test.notes.di.scopes.ActivityScope
import com.test.notes.model.db.NotesDatabase
import com.test.notes.ui.MainActivity
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Dashkevich Makar on 19/06/2020.
 * mcrena@mail.ru
 */
@Module(includes = [AndroidSupportInjectionModule::class, DatabaseModule::class])
abstract class AppModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun mainActivity(): MainActivity

}