package com.test.notes.di.modules

import com.test.notes.di.scopes.FragmentScope
import com.test.notes.ui.edit.EditNoteFragment
import com.test.notes.ui.home.HomeFragment
import com.test.notes.ui.note_view.NoteViewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Dashkevich Makar on 19/06/2020.
 * mcrena@mail.ru
 */
@Module
abstract class MainActivityModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun homeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun editFragment(): EditNoteFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun viewNoteFragment(): NoteViewFragment

}