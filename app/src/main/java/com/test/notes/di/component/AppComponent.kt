package com.test.notes.di.component

import android.content.Context
import com.test.notes.core.NotesApplication
import com.test.notes.di.modules.AppModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Dashkevich Makar on 19/06/2020.
 * mcrena@mail.ru
 */
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }

    fun inject(app: NotesApplication)
}