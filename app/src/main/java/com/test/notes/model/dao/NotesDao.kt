package com.test.notes.model.dao

import androidx.room.*
import com.test.notes.model.entity.Note
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by Dashkevich Makar on 19/06/2020.
 * mcrena@mail.ru
 */

@Dao
interface NotesDao {

    @Query("SELECT * FROM note")
    fun getAll(): List<Note>

    @Query("SELECT * FROM note WHERE id=:id")
    fun getById(id: Int): Note

    @Update
    fun update(note: Note)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(note: Note):Long

    @Query("DELETE FROM note WHERE id=:id")
    fun deleteById(id: Int)
}