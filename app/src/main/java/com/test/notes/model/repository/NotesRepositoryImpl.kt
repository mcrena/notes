package com.test.notes.model.repository

import com.test.notes.model.db.NotesDatabase
import com.test.notes.model.entity.Note
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Dashkevich Makar on 22/06/2020.
 * mcrena@mail.ru
 */
class NotesRepositoryImpl constructor(val db: NotesDatabase) : NotesRepository {

    override fun getAll(): Single<List<Note>> {
        return Single.create {
            it.onSuccess(db.notesDao().getAll())
        }
    }

    override fun getById(id: Int): Single<Note> {
        return Single.create {
            it.onSuccess(db.notesDao().getById(id))
        }
    }

    override fun update(note: Note): Completable {
        return Completable.create { db.notesDao().update(note) }
    }

    override fun insert(note: Note): Completable {
        return Completable.create {
            db.notesDao().insert(note)
            it.onComplete()
        }
    }

    override fun deleteById(id: Int): Completable {
        return Completable.create {
            db.notesDao().deleteById(id)
            it.onComplete()
        }
    }

}