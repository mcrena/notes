package com.test.notes.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

/**
 * Created by Dashkevich Makar on 19/06/2020.
 * mcrena@mail.ru
 */
@Entity
data class Note(
    val title: String,
    val content: String,
    val imageUrl: String?
) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    fun equals(other: Note): Boolean {
        return title == other.title && content == other.content && imageUrl == other.imageUrl && id == other.id

    }
}