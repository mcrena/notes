package com.test.notes.model.repository

import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.test.notes.model.entity.Note
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by Dashkevich Makar on 22/06/2020.
 * mcrena@mail.ru
 */
interface NotesRepository {
    fun getAll(): Single<List<Note>>

    fun getById(id: Int): Single<Note>

    fun update(note: Note): Completable

    fun insert(note: Note): Completable

    fun deleteById(id: Int): Completable
}