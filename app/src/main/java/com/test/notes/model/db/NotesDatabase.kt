package com.test.notes.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.test.notes.model.dao.NotesDao
import com.test.notes.model.entity.Note

/**
 * Created by Dashkevich Makar on 19/06/2020.
 * mcrena@mail.ru
 */
@Database(entities = [Note::class], version = 1, exportSchema = false)
abstract class NotesDatabase : RoomDatabase() {
    abstract fun notesDao(): NotesDao
}