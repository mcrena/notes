package com.test.notes.ui.home

import com.test.notes.model.entity.Note
import com.test.notes.mvp.BasePresenter
import com.test.notes.mvp.BaseView
import moxy.InjectViewState
import moxy.MvpPresenter
import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution
import moxy.viewstate.strategy.alias.SingleState

/**
 * Created by Dashkevich Makar on 20/06/2020.
 * mcrena@mail.ru
 */
interface HomeContract {

    interface HomeView : BaseView {

        @AddToEndSingle
        fun showNotes(notes: List<Note>)

        @AddToEndSingle
        fun showEmpty(show: Boolean)
    }
}