package com.test.notes.ui.base

import android.os.Bundle
import android.view.MenuItem
import com.test.notes.di.modules.MainActivityModule
import com.test.notes.ui.MainActivity

/**
 * Created by Dashkevich Makar on 20/06/2020.
 * mcrena@mail.ru
 */
abstract class BaseInnerFragment : BaseFragment() {

    override fun onResume() {
        super.onResume()
        initFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    abstract val title: String

    private fun initFragment() {
        setFabVisible(false)
        val activity = this.activity as MainActivity
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        activity.supportActionBar?.title = title
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            activity?.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}