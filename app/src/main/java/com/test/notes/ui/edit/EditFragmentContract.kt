package com.test.notes.ui.edit

import android.net.Uri
import com.test.notes.model.entity.Note
import com.test.notes.mvp.BaseView
import moxy.viewstate.strategy.alias.AddToEndSingle
import moxy.viewstate.strategy.alias.OneExecution

/**
 * Created by Dashkevich Makar on 20/06/2020.
 * mcrena@mail.ru
 */
interface EditFragmentContract {

    interface EditView : BaseView {
        @OneExecution
        fun fillNote(note: Note)

        @AddToEndSingle
        fun showPhoto(uri: Uri?)

        @OneExecution
        fun exit()
    }
}