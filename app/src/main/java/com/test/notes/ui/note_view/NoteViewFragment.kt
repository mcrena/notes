package com.test.notes.ui.note_view

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.test.notes.R
import com.test.notes.model.entity.Note
import com.test.notes.ui.base.BaseInnerFragment
import kotlinx.android.synthetic.main.note_view_frg.*

/**
 * Created by Dashkevich Makar on 20/06/2020.
 * mcrena@mail.ru
 */
class NoteViewFragment : BaseInnerFragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.note_view_frg, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val note = arguments?.getSerializable("note") as Note?
        note?.let {
            txtTitle.text = it.title
            txtNoteContent.text = it.content
            note.imageUrl?.let { imageUrl ->
                val uri = Uri.parse(imageUrl)
                Picasso.get().load(uri).into(imgNotePhoto)
            }
        }
    }

    override val title: String
        get() = getString(R.string.view_note)
}