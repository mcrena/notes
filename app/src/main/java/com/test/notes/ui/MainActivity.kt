package com.test.notes.ui

import android.os.Bundle
import androidx.navigation.findNavController
import com.test.notes.R
import com.test.notes.ui.base.BaseActivity

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        fab.setOnClickListener {
            findNavController(R.id.nav_host_fragment).navigate(R.id.action_HomeFragment_to_EditFragment)
        }
    }
}
