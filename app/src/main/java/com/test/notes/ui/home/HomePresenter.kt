package com.test.notes.ui.home

import android.net.Uri
import com.test.notes.model.db.NotesDatabase
import com.test.notes.model.entity.Note
import com.test.notes.model.repository.NotesRepository
import com.test.notes.mvp.BasePresenter
import com.test.notes.tools.resultOnMainThread
import io.reactivex.Completable
import moxy.InjectViewState
import java.io.File
import javax.inject.Inject

/**
 * Created by Dashkevich Makar on 20/06/2020.
 * mcrena@mail.ru
 */
@InjectViewState
class HomePresenter @Inject constructor() :
    BasePresenter<HomeContract.HomeView>() {

    @Inject
    lateinit var notesRepository: NotesRepository

    override fun attachView(view: HomeContract.HomeView?) {
        super.attachView(view)
        getAllNotes()
    }

    private fun getAllNotes() {
        addDisposable(
            notesRepository.getAll()
                .resultOnMainThread()
                .subscribe({
                    viewState.showEmpty(it.isNullOrEmpty())
                    viewState.showNotes(it)
                },
                    //onError
                    {

                    })
        )

    }

    fun deleteNote(note: Note) {
        note.imageUrl?.let {
            val uri = Uri.parse(it)
            Completable.create {
                val file = File(uri.path)
                file.delete()
            }.resultOnMainThread().subscribe()

        }
        addDisposable(
            notesRepository.deleteById(note.id)
                .resultOnMainThread()
                .subscribe {
                    getAllNotes()
                })
    }
}