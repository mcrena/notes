package com.test.notes.ui.base

import android.os.Bundle
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.test.notes.R
import com.test.notes.mvp.BaseView
import com.test.notes.ui.MainActivity
import dagger.android.support.AndroidSupportInjection
import moxy.MvpAppCompatFragment

/**
 * Created by Dashkevich Makar on 19/06/2020.
 * mcrena@mail.ru
 */
open class BaseFragment : MvpAppCompatFragment(), BaseView {


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun showSnackbar(message: String) {
        view?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG).show()
        }
    }

    override fun showToast(resId: Int) {
        Toast.makeText(requireContext(), resId, Toast.LENGTH_SHORT).show()
    }

    fun setFabVisible(visible: Boolean) {
        activity?.findViewById<View>(R.id.fab)?.visibility =
            if (visible) VISIBLE else INVISIBLE
    }
}