package com.test.notes.ui.edit

import android.net.Uri
import com.test.notes.R
import com.test.notes.model.db.NotesDatabase
import com.test.notes.model.entity.Note
import com.test.notes.model.repository.NotesRepository
import com.test.notes.mvp.BasePresenter
import com.test.notes.tools.resultOnMainThread
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import moxy.InjectViewState
import javax.inject.Inject

/**
 * Created by Dashkevich Makar on 20/06/2020.
 * mcrena@mail.ru
 */
@InjectViewState
class EditNotePresenter @Inject constructor() : BasePresenter<EditFragmentContract.EditView>() {

    @Inject
    lateinit var notesRepository: NotesRepository

    var noteId: Int? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        noteId?.let {
            getNote(it)
        }

    }

    private fun getNote(noteId: Int) {
        compositeDisposable.add(
            notesRepository.getById(noteId)
                .resultOnMainThread()
                .subscribe({
                    viewState.fillNote(it)
                },
                    //onError
                    {

                    })
        )
    }

    fun setNoteId(noteId: Int) {
        this.noteId = noteId
    }

    fun saveNote(noteTitle: String, noteContent: String, uri: Uri?) {
        if (noteTitle.isBlank()) {
            viewState.showToast(R.string.title_is_empty)
            return
        }
        if (noteContent.isBlank()) {
            viewState.showToast(R.string.content_is_empty)
            return
        }
        val note = Note(title = noteTitle, content = noteContent, imageUrl = uri?.toString())
        noteId?.let {
            note.id = it
        }
        addDisposable(
            notesRepository
                .insert(note)
                .resultOnMainThread()
                .subscribe {
                    viewState.showToast(R.string.note_save_success)
                    viewState.exit()
                }
        )
    }

    fun getPhoto(single: Single<Uri>) {
        addDisposable(single.resultOnMainThread().subscribe({
            viewState.showPhoto(it)
        }, {}))
    }
}