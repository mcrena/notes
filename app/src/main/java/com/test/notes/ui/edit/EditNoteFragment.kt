package com.test.notes.ui.edit

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.test.notes.R
import com.test.notes.model.entity.Note
import com.test.notes.tools.*
import com.test.notes.ui.base.BaseInnerFragment
import com.test.notes.ui.home.HomePresenter
import kotlinx.android.synthetic.main.edit_note_frg.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class EditNoteFragment : BaseInnerFragment(), EditFragmentContract.EditView {

    @Inject
    @InjectPresenter
    lateinit var presenter: EditNotePresenter

    @ProvidePresenter
    fun provide(): EditNotePresenter = presenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.edit_note_frg, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.getInt("noteId")?.let {
            presenter.setNoteId(noteId = it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (cameraPermissionGranted()) {
            startCamera()
        } else {
            requestPermissions(REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }
        btnSave.setOnClickListener {
            presenter.saveNote(
                edtTitle.text.toString(),
                edtContent.text.toString(),
                imgNotePhoto.tag as Uri?
            )
        }
        btnTakePhoto.setOnClickListener {
            presenter.getPhoto(takePhoto(requireContext()))
        }
    }

    private fun startCamera() {
        cameraContainer.visibility = VISIBLE
        startCamera(this, viewFinder)
    }

    override fun onResume() {
        super.onResume()
        setFabVisible(false)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (cameraPermissionGranted()) {
            startCamera()
        } else {
            showToast(R.string.permissions_needed)
            cameraContainer.visibility = GONE
        }
    }

    override val title: String
        get() = if (arguments?.getInt("noteId") != null) getString(R.string.edit_note) else getString(
            R.string.new_note
        )

    override fun fillNote(note: Note) {
        edtTitle.setText(note.title)
        edtContent.setText(note.content)
        note.imageUrl?.let {
            val uri = Uri.parse(it)
            showPhoto(uri)
        }

    }

    override fun showPhoto(uri: Uri?) {
        uri?.let {
            imgNotePhoto.visibility = VISIBLE
            Picasso.get().load(uri).into(imgNotePhoto)
            imgNotePhoto.tag = uri
        }
    }

    override fun exit() {
        activity?.onBackPressed()
    }
}
