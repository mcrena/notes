package com.test.notes.ui.adapters

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.test.notes.R
import com.test.notes.model.entity.Note
import kotlinx.android.synthetic.main.note_listitem.view.*

/**
 * Created by Dashkevich Makar on 20/06/2020.
 * mcrena@mail.ru
 */
class NotesRecyclerAdapter(private val listener: NotesAdapterEvents) :
    RecyclerView.Adapter<NotesRecyclerAdapter.ViewHolder>() {

    interface NotesAdapterEvents {
        fun onDeleteClick(note: Note)

        fun onEditClick(note: Note)

        fun onItemClick(note: Note)
    }

    var items: List<Note> = ArrayList()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.note_listitem,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note = items[position]
        with(holder.itemView) {
            this.setOnClickListener { listener.onItemClick(note) }
            txtTitle.text = note.title
            txtNoteContent.text = note.content
            btnEdit.setOnClickListener { listener.onEditClick(note) }
            btnDelete.setOnClickListener { listener.onDeleteClick(note) }
            imgNotePhoto.visibility = if (note.imageUrl == null) GONE else VISIBLE
            note.imageUrl?.let {
                val uri = Uri.parse(it)
                Picasso.get().load(uri).into(imgNotePhoto)
            }
        }
    }
}