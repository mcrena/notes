package com.test.notes.ui.home

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.test.notes.R
import com.test.notes.model.entity.Note
import com.test.notes.ui.MainActivity
import com.test.notes.ui.adapters.NotesRecyclerAdapter
import com.test.notes.ui.base.BaseFragment
import com.test.notes.ui.edit.EditNotePresenter
import kotlinx.android.synthetic.main.home_frg.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class HomeFragment : BaseFragment(), HomeContract.HomeView,
    NotesRecyclerAdapter.NotesAdapterEvents {

    @Inject
    @InjectPresenter
    lateinit var presenter: HomePresenter


    @ProvidePresenter
    fun provide(): HomePresenter = presenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.home_frg, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv.adapter = NotesRecyclerAdapter(this)
    }

    override fun onResume() {
        super.onResume()
        setFabVisible(true)
        val activity = this.activity as MainActivity
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        activity.supportActionBar?.title = getString(R.string.my_notes)
    }

    override fun showNotes(notes: List<Note>) {
        val adapter = rv.adapter as NotesRecyclerAdapter
        adapter.items = notes
        adapter.notifyDataSetChanged()
    }

    override fun showEmpty(show: Boolean) {
        txtEmpty.visibility = if (show) VISIBLE else GONE
    }


    override fun onDeleteClick(note: Note) {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(R.string.delete_this_note)
        builder.setPositiveButton(R.string.delete) { i, _ ->
            presenter.deleteNote(note)
            i.dismiss()
        }
        builder.setNegativeButton(R.string.cancel) { i, _ ->
            i.dismiss()
        }
        builder.show()
    }

    override fun onEditClick(note: Note) {
        val bundle = Bundle()
        bundle.putInt("noteId", note.id)
        findNavController().navigate(R.id.action_HomeFragment_to_EditFragment, bundle)
    }

    override fun onItemClick(note: Note) {
        val bundle = Bundle()
        bundle.putSerializable("note", note)
        findNavController().navigate(R.id.noteViewFragment, bundle)
    }
}
