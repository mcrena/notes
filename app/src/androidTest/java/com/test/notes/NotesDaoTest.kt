package com.test.notes

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.test.notes.model.dao.NotesDao
import com.test.notes.model.db.NotesDatabase
import com.test.notes.model.entity.Note
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Dashkevich Makar on 22/06/2020.
 * mcrena@mail.ru
 */
@RunWith(AndroidJUnit4::class)
class NotesDaoTest {
    lateinit var db: NotesDatabase
    lateinit var notesDao: NotesDao

    @Before
    fun initDb() {
        db = Room.databaseBuilder(
            androidx.test.platform.app.InstrumentationRegistry.getInstrumentation().targetContext,
            NotesDatabase::class.java,
            "database"
        ).build()
        notesDao = db.notesDao()
    }

    @Test
    @Throws
    fun insertNoteTest() {
        val note = Note("title", "Content", null)
        val insertedId = db.notesDao().insert(note)
        val insertedNote = db.notesDao().getById(insertedId.toInt())
        assert(note.equals(insertedNote))
    }

    @Test
    @Throws
    fun updateNoteTest() {
        val noteId = db.notesDao().getAll()[0].id
        val updatedNote = Note("newTitle", "newContent", null).apply { id = noteId }
        db.notesDao().insert(updatedNote)
        val newNote = db.notesDao().getById(noteId)
        assert(newNote.title == "newTitle" && newNote.content == "newContent")
    }

    @After
    fun close() {
        db.close()
    }
}